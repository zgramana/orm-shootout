using System;
using SubSonic.SqlGeneration.Schema;

namespace ITCrowd
{
    public enum ShowCharacter
    {
        Moss,
        Jen,
        Roy,
        Reynholm,
        Denholm,
        Richmond
    }

    [Flags]
    public enum Series
    {
        One = 1,
        Two = 2,
        Three = 4,
        Four = 8,
        Five = 16
    }

    public class ShowCredit
    {
       private Series _activeSeasons;
    	private ShowCharacter _character;
       public Int32 ID {get;set;}
	   public ShowCharacter Character { get { return _character; } set { _character = value; } }
	   [SubSonicIgnore]
	   public Int32 CharacterID
	   {
		   get { return (Int32)_character; }
		   set { Enum.ToObject(typeof(ShowCharacter), _character); }
	   }
	   public String PerformersName { get; set; }
       public Series ActiveSeasons { get { return _activeSeasons; } set { _activeSeasons = value; } }
       [SubSonicIgnore]
       public String ActiveSeasonsString { 
           get { return ActiveSeasons.ToString(); }
           set { Enum.TryParse(value, false, out _activeSeasons); }
       }
    }
}