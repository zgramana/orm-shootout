using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using ITCrowd;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SubSonic.DataProviders;
using SubSonic.Repository;

namespace Spike.Tests
{
	[TestClass]
	public class FlaggedEnumUnitTests
	{
		private readonly String _connString = @"Data Source=VMW7A\SQLEXPRESS;Initial Catalog=Qoo;Integrated Security=True";

		[TestInitializeAttribute()]
		public void Test_Setup()
		{
			// For SQL Server to warm up, and ADO.NET.
			DataReader_FlaggedEnumTest();
		}

		[TestMethod]
		public void DataReader_FlaggedEnumTest() 
		{
			Console.WriteLine(" >>> Doing DataReader lookup.");
			var timer = new Stopwatch();
			timer.Start();
			var conn = new SqlConnection(_connString);
			conn.Open();
			var command = conn.CreateCommand();
			command.CommandText = @"SELECT * FROM ShowCredits";
			var reader = command.ExecuteReader();
			var results = new List<ShowCredit>();
			while (reader.Read())
			{
				var credit = new ShowCredit
				             	{
				             		ID = (int)reader["ID"],
				             		Character = (ShowCharacter)Enum.ToObject(typeof(ShowCharacter), reader["Character"]),
				             		PerformersName = reader["PerformersName"] as String
				             	};
				Series seasons;
				Enum.TryParse(reader["ActiveSeasons"] as String, false, out seasons);
				credit.ActiveSeasons = seasons;
				results.Add(credit);
			}
			timer.Stop();
			reader.Close();
			reader.Dispose();
			command.Dispose();
			conn.Close();
			conn.Dispose();
			var acredit = results.FirstOrDefault();
			Console.WriteLine(String.Format(">>> DataReader lookup took {0:D} milliseconds.", timer.ElapsedMilliseconds));
			Console.WriteLine(">>> Found a credit for {0}, who played {1} during Seasons {2}.", acredit.PerformersName, acredit.Character, acredit.ActiveSeasons);
		}

		[TestMethod]
		public void SubSonic_FlaggedEnumTest()
		{
			var providerName = "System.Data.SqlClient";

			var factory = ProviderFactory.GetProvider(_connString, providerName);
			var repo = new SimpleRepository(factory, SimpleRepositoryOptions.RunMigrations);

			ShowCredit credit = null;
			try
			{

				Console.WriteLine(" >>> Doing SubSonic lookup.");
				var timer = new Stopwatch();
				timer.Start();
				credit = repo.Single<ShowCredit>(c => c.Character == ShowCharacter.Jen);
				credit = null;
				credit = repo.Single<ShowCredit>(c => c.Character == ShowCharacter.Roy || c.Character == ShowCharacter.Moss );
				timer.Stop();
				Console.WriteLine(String.Format("Took {0:D} milliseconds.", timer.ElapsedMilliseconds));
			}
			catch (Exception ex)
			{
				Console.WriteLine(" !!! Error encountered: {0}", ex.Message);
				Assert.Fail(ex.InnerException != null ? ex.Message + "\r\n" + ex.InnerException.Message : ex.Message);
			}

			if (credit == null)
			{
				Console.WriteLine(" !!! No credit found.");
				var newCredit = new ShowCredit
				{
					Character = ShowCharacter.Moss,
					PerformersName = "Richard Ayoade",
					ActiveSeasons = Series.One | Series.Two | Series.Three | Series.Four // "One, Two, Three, Four" //
				};
				try
				{
					Console.WriteLine(" >>> Adding new credit.");
					repo.Add(newCredit);
				}
				catch (Exception ex)
				{
					Console.WriteLine(" !!! Error encountered: {0}", ex.Message);
					Console.WriteLine(ex.StackTrace);
					Assert.Fail(ex.InnerException != null ? ex.Message + "\r\n" + ex.InnerException.Message : ex.Message);
				}
				return;
			}

			Console.WriteLine(" >>> Found a credit for {0}, who played {1} during Seasons {2}.", credit.PerformersName, credit.Character, credit.ActiveSeasons);
		}

		[TestMethod]
		public void EF_FlaggedEnumTest()
		{
			var repo = new ITCrowdRepo(_connString);

			ShowCredit credit = null;
			try
			{

				Console.WriteLine(" >>> Doing EF lookup.");
				var timer = new Stopwatch();
				timer.Start();
				credit = repo.Credits.ToList().SingleOrDefault<ShowCredit>(c => c.CharacterID == (int)ShowCharacter.Jen);
				credit = null;
				credit = repo.Credits.SingleOrDefault<ShowCredit>(c => c.CharacterID == (int)ShowCharacter.Roy || c.CharacterID == (int)ShowCharacter.Moss);
				timer.Stop();
				Console.WriteLine(String.Format("Took {0:D} milliseconds.", timer.ElapsedMilliseconds));
			}
			catch (Exception ex)
			{
				Console.WriteLine(" !!! Error encountered: {0}", ex.Message);
				Assert.Fail(ex.InnerException != null ? ex.Message + "\r\n" + ex.InnerException.Message + "\r\n" + ex.StackTrace : ex.Message + "\r\n" + ex.StackTrace);
			}

			if (credit == null)
			{
				Console.WriteLine(" !!! No credit found.");
				var newCredit = new ShowCredit
				{
					Character = ShowCharacter.Moss,
					PerformersName = "Richard Ayoade",
					ActiveSeasons = Series.One | Series.Two | Series.Three | Series.Four // "One, Two, Three, Four" //
				};
				try
				{
					Console.WriteLine(" >>> Adding new credit.");
					repo.Credits.Add(newCredit);
					repo.SaveChanges();
				}
				catch (Exception ex)
				{
					Console.WriteLine(" !!! Error encountered: {0}", ex.Message);
					Console.WriteLine(ex.StackTrace);
					Assert.Fail(ex.InnerException != null ? ex.Message + "\r\n" + ex.InnerException.Message : ex.Message);
				}
				return;
			}

			Console.WriteLine(" >>> Found a credit for {0}, who played {1} during Seasons {2}.", credit.PerformersName, credit.Character, credit.ActiveSeasons);			
		}

	}
}
