using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using ITCrowd;

namespace Spike.Tests
{
	public sealed class ITCrowdRepo : DbContext
	{
		public DbSet<ShowCredit> Credits { get; set; }

		public ITCrowdRepo(String connectionString) : base(connectionString) { }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new ShowCreditConfiguration());

			base.OnModelCreating(modelBuilder);
		}
	}

	public class ShowCreditConfiguration : EntityTypeConfiguration<ShowCredit>
	{
		public ShowCreditConfiguration()
		{
			HasKey(sc => sc.ID)
				.ToTable("ShowCredits");
			Property(sc => sc.CharacterID)
				.HasColumnName("Character");
			Property(sc => sc.ActiveSeasonsString)
				.HasColumnName("ActiveSeasons");
			Ignore(sc => sc.Character);
			Ignore(sc => sc.ActiveSeasons);
		}
	}
}
